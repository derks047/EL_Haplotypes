#!/usr/bin/env python
## Martijn Derks
## Count all existing haplotypes in sliding window approach report observed and expected homozygotes.

import argparse
from collections import OrderedDict
import sys
import subprocess
import gzip
import numpy as np
from scipy.stats import binom_test
import pprint
from math import e
from scipy import stats

parser = argparse.ArgumentParser( description='Analyse missing homozygote haplotypes to identify lethal recessives using phased population data')
parser.add_argument("-v", "--vcf_file", help="phased VCF_file (compressed)", nargs=1)
parser.add_argument("-p", "--pedigree", help="Pedigree (pig_id sire dam sex), space delimited", nargs=1)
parser.add_argument("-o", "--output_file", help="Output file", nargs=1)
parser.add_argument("-w", "--window_size", help="Window size to build haplotypes (bp)", nargs=1)

class HF:

	def __init__(self,outpath,windowsize):
		""" Set class variables """
		self.outpath=outpath
		self.windowsize=int(windowsize)

	def get_chr_length(self,vcffile):
		""" Get the chromosome lengths """
		self.length = subprocess.check_output("zcat "+vcffile+" | tail -1 | cut -f2 ", shell=True)
		self.chromosome = subprocess.check_output("zcat "+vcffile+" | tail -1 | cut -f1 ", shell=True).strip()
		self.chr = "Chr"+self.chromosome.strip()

	def get_vcf_samples(self,vcffile):
		""" Get all the phased samples and write in sample dictionary """
		cmd = "zcat "+vcffile+" | grep -i '#CHROM'"
		samples = subprocess.check_output(cmd, shell=True)
		self.sample_dic = {}
		scount = 0
		sample_list = samples.strip().split("\t")[9:]
		for sample in sample_list:
			self.sample_dic[scount] = sample
			self.sample_dic[scount+1] = sample
			scount += 2

	def sliding_windows(self, genotype_dic):
		""" Scan the haplotypes per sliding window """
		self.outfile=open(self.outpath,"w")
		header = ["Chr", "Start","Stop","Haplotype","Genotype","Markers","Haplotype_length","#Haplotypes_in_window","Frequencies_haplotypes_in_window","Haplotype_count","#Homozygous","Haplotype_frequency","#Sires","#Dams","Homozygotes_expected_(simple)","Homozygotes_expected_(pedigree)","Carrier_matings","#Pedigree_carrier_matings","Excact_binomial_test","Carrier_matings","#Carrier_progeny","Carrier_progeny","#Heterozygote_progeny","Heterozygote_progeny"]
		self.outfile.write("\t".join(header)+"\n")
		#int(self.length)
		for start in range(0, int(self.length), int(self.windowsize*0.5)):
			self.marker_dic = {}
			stop = start+self.windowsize
			self.window_key = str(self.chromosome)+"_"+str(start)+"_"+str(stop)
			hap_dic={}
			for marker in sorted(genotype_dic.keys()):
				if int(marker) >= start and int(marker) <= stop:
					snp=genotype_dic[marker]
					count=0
					self.marker_dic[marker] = [snp[1],snp[3],snp[4]]
					for gt in snp[9:]:
						gt=gt.split("|")
						if count in hap_dic:
								hap_dic[count].append(gt[0])
								hap_dic[count+1].append(gt[1][:1])
						else:
								hap_dic[count]=[gt[0]]
								hap_dic[count+1]=[gt[1][:1]]
						count+=2
			hap_freq_dic,sample_dic,zygosity_dic,hap_homozygosity_dic = self.get_frequencies(hap_dic, start, stop)
			self.window_statistics(hap_freq_dic)
			self.summarize_hap_analysis_and_report(hap_freq_dic,sample_dic,zygosity_dic,hap_homozygosity_dic,start,stop)

	def read_haps_vcffile(self, vcffile):
		""" Parse the phased file from beagle in a genotype dictionary """
		genotype_dic={}
		with gzip.open(vcffile,'r') as hapsfile:
			for snp in hapsfile:
				snp=snp.strip()
				if not snp.startswith("#"):
					snp=snp.strip().split()
					snp_pos = int(snp[1])
					genotype_dic[snp_pos] = snp
		hapsfile.close()
		return genotype_dic

	def sequenced_samples(self):
		""" Get all the sequenced samples """
		self.sample_seq_dic = {}
		self.seq_samples_genotyped = []
		sample_seq_file = open("/lustre/backup/WUR/ABGC/shared/ABGC_Projects/STW_Deleterious_Alleles/Martijn_PHD/Data/Pig/SampleIDs_Animal_Ids.tsv","r")	
		header=sample_seq_file.readline()
		for sample in sample_seq_file:
			sample=sample.strip().split("\t")
			self.sample_seq_dic[sample[1]] = sample[0]		

	def get_frequencies(self, hap_dic, start, stop):
		""" Count haplotype frequencies and check for homozygosity """
		hap_freq_dic,sample_dic,zygosity_dic,hap_homozygosity_dic={},{},{},{}
		count,self.thap =0,0
		for hap in hap_dic:
			haplotype="".join(hap_dic[hap])
			if haplotype in sample_dic: ## Get all the samples with this haplotype
				sample_dic[haplotype].append(self.sample_dic[hap])
			else:
				sample_dic[haplotype] = [self.sample_dic[hap]]

			if haplotype in hap_freq_dic: ## Count haplotype occurrence
				hap_freq_dic[haplotype] += 1
			else:
				hap_freq_dic[haplotype] = 1
			self.thap += 1
			if count == 1: ## Check if haplotype occurs in homozygous state
				if haplotype == "".join(hap_dic[hap-1]):
					if haplotype in hap_homozygosity_dic:
						hap_homozygosity_dic[haplotype] += 1
					else:
						hap_homozygosity_dic[haplotype] = 1
					if haplotype in zygosity_dic:
							zygosity_dic[haplotype][0].append(self.sample_dic[hap])
					else:
							zygosity_dic[haplotype] = [[self.sample_dic[hap]],[]]
				else:
					if haplotype in zygosity_dic:
							zygosity_dic[haplotype][1].append(self.sample_dic[hap])
					else:
						zygosity_dic[haplotype] = [[],[self.sample_dic[hap]]]
					shaplotype = "".join(hap_dic[hap-1])
					if shaplotype in zygosity_dic:
							zygosity_dic[shaplotype][1].append(self.sample_dic[hap-1])
					else:
							zygosity_dic[shaplotype] = [[],[self.sample_dic[hap-1]]]
				count=0
			else:
				count +=1
		return hap_freq_dic,sample_dic,zygosity_dic,hap_homozygosity_dic

	def window_statistics(self, hap_freq_dic):
		""" Get window statistics from haplotypes """
		self.window_haplotype_frequencies={}
		hap_freq_dic = OrderedDict(sorted(hap_freq_dic.items(), key=lambda x: x[1]))
		for hap in hap_freq_dic:
			tcount = hap_freq_dic[hap]
			cfreq = float(tcount)/self.thap
			if cfreq > 0.005:
					self.window_haplotype_frequencies[hap] = round(cfreq,3)
		
	def summarize_hap_analysis_and_report(self, hap_freq_dic,sample_dic,zygosity_dic,hap_homozygosity_dic,start,stop): 
		""" Report output """
		tcount=0
		hap_freq_dic = OrderedDict(sorted(hap_freq_dic.items(), key=lambda x: x[1]))	
		for hap in hap_freq_dic:
			tcount = hap_freq_dic[hap]
			cfreq = float(tcount)/self.thap
			tanimals = self.thap/2
			homozygous_expected = tanimals * (cfreq**2) ## expected homozygotes based on frequency
			seq_sample_list = []
			for sample in sample_dic[hap]:
				if sample in self.sample_seq_dic:
					if not self.sample_seq_dic[sample] in seq_sample_list:
						seq_sample_list.append(self.sample_seq_dic[sample])
			if seq_sample_list == []:
				seq_sample_list = ["-"]
			observed_homozygotes = len(zygosity_dic[hap][0])
			if cfreq >= 0.005  and observed_homozygotes <= 5: ## Get missing homozygotes
				number_sires, number_dam, hap_hom_expect_total,carrier_matings,exact_binomial_test,total_carrier_progeny,heterozygote_progeny = self.est_homozygotes(hap, zygosity_dic, sample_dic)
				if homozygous_expected >= 5: # and exact_binomial_test < 0.05: ## Get significant 
					if 1==2: #observed_homozygotes > 0 and exact_binomial_test > 0.0001: ## Do not output haplotypes with deficit homozygosity with low p-value.
						continue
					carriers = sample_dic[hap]

					if not hap in hap_homozygosity_dic:
						nhomozygous = 0
					else:
						nhomozygous = hap_homozygosity_dic[hap]

					heterozygote_progeny_count = len(heterozygote_progeny)
					if heterozygote_progeny == []:
							heterozygote_progeny = ["-"]
					if total_carrier_progeny == []:
							total_carrier_progeny = ["-"]
					markers, genotypes = self.get_haplotype_SNPs(hap)
					
					outlist = [str(self.chr), str(start), str(stop), str(hap), genotypes, markers, str(len(hap)), str(len(self.window_haplotype_frequencies)), ",".join(map(str, sorted(self.window_haplotype_frequencies.values(), reverse=True))), str(tcount), str(nhomozygous), str("%.3f" % cfreq), str(number_sires), str(number_dam), str("%.2f" % homozygous_expected), str(hap_hom_expect_total), str(carrier_matings), \
					str(exact_binomial_test), ",".join(seq_sample_list), str(len(total_carrier_progeny)), ",".join(total_carrier_progeny), str(heterozygote_progeny_count), ",".join(heterozygote_progeny), ",".join(carriers)]
					self.outfile.write("\t".join(outlist)+"\n")
					
	def get_haplotype_SNPs(self, hap):
		""" Get the markers that make up the haplotype """
		markers,genotypes = [],[]
		i = 0
		for gt in hap:
			marker = sorted(self.marker_dic.keys())[i]
			alleles = "".join(self.marker_dic[marker][1:])
			if int(gt) == 0:
					genotypes.append(self.marker_dic[marker][1])
			else:
					genotypes.append(self.marker_dic[marker][2])
			i+=1
			markers.append(self.marker_dic[marker][0]+"_"+alleles)
	
		return ",".join(markers), "".join(genotypes)


	def est_homozygotes(self, haplotype, zygosity_dic, sample_dic):
		""" Count haplotype frequencies and check for homozygosity """
		samples = sample_dic[haplotype]
		total_carrier_progeny, heterozygote_progeny = [], []
		nsires = len(set(samples) & set(self.sire))
		ndam = len(set(samples) & set(self.dam))
		hap_hom_expect_total,total_progeny,carrier_matings = 0,0,0
		obs,exp=[],[]
		homozygotes = zygosity_dic[haplotype][0]
		heterozygotes = zygosity_dic[haplotype][1]
		for parents in self.parent_dic:
			homozygotes_number_obs = 0
			sire, dam = parents.split("_")
			if sire in zygosity_dic[haplotype][0]:
				sire_t_probability = 1
			elif sire in zygosity_dic[haplotype][1]:
				sire_t_probability = 0.5
			else:
				sire_t_probability = 0

			if dam in zygosity_dic[haplotype][0]:
					dam_t_probability = 1
			elif dam in zygosity_dic[haplotype][1]:
					dam_t_probability = 0.5
			else:
					dam_t_probability = 0

			progeny = len(self.parent_dic[parents])
			progeny_samples = self.parent_dic[parents]
			for sample in progeny_samples:
				if sample in homozygotes:
					homozygotes_number_obs += 1
			hap_hom_expect = (sire_t_probability*dam_t_probability)*progeny
			if sire_t_probability == 0.5 and dam_t_probability == 0.5: ## Carrier matings
				carrier_matings += 1
				for sample in progeny_samples:
					if sample in self.sample_dic.values(): ## Added
						total_carrier_progeny.append(sample)
						if sample in heterozygotes:
								heterozygote_progeny.append(sample)

			hap_hom_expect_total += hap_hom_expect
			if hap_hom_expect == 0 and homozygotes_number_obs == 0:
				pass
			else:  
				total_progeny += progeny
				exp.append(float(hap_hom_expect))
				obs.append(float(homozygotes_number_obs))
		if sum(obs) < sum(exp):
			probability_succes=sum(exp)/total_progeny

			""" Perform excact binomial test """
			pvalue=binom_test(sum(obs),total_progeny,probability_succes)
		else:
			pvalue="nan"

		return nsires,ndam,hap_hom_expect_total,carrier_matings,pvalue,total_carrier_progeny,heterozygote_progeny

	def pedigree(self, ped):
		""" Parse pedigree """
		self.peddic = {}
		self.sire = []
		self.dam = []
		self.crossbreds = []
		pedfile = open(ped,"r")
		self.parent_dic={}
		header = pedfile.readline()
		for individual in pedfile:
			individual=individual.strip().split(" ") ## Remove comma for chicken pedigree
			sample = individual[0]
			if not sample in self.peddic:
				self.peddic[sample] = [individual[1], individual[2]]
				if individual[1] != 0 and individual[1] not in self.sire:
					self.sire.append(individual[1])
				if individual[2] != 0 and individual[2] not in self.dam:
					self.dam.append(individual[2])

				if individual[1] in self.sample_dic.values() and individual[2] in self.sample_dic.values(): 
					parents = individual[1]+"_"+individual[2]
					if sample in self.sample_dic.values():
						if parents in self.parent_dic:
							self.parent_dic[parents].append(sample)
						else:
							self.parent_dic[parents]=[sample]
		pedfile.close()

args = parser.parse_args()
vcffile = args.vcf_file[0]
pedigree = args.pedigree[0]
outfile = args.output_file[0]
windowsize = int(args.window_size[0])

H=HF(outfile,windowsize)
H.get_chr_length(vcffile)
H.get_vcf_samples(vcffile)
H.sequenced_samples()
H.pedigree(pedigree)
genotype_dic=H.read_haps_vcffile(vcffile)
H.sliding_windows(genotype_dic)

