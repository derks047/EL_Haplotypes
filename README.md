Pipeline to report haplotypes using a sliding window approach. The pipeline reports observed and expected homozygotes for each haplotype. Haplotypes showing a deficit in homozygosity could carry a lethal recessive allele. 

The script reads a phased vcf file and pedigree file (whitespace delimited: Individual, Sire, Sam, Sex (M/F)).

usage: pipeline_HHD_basic.py [-h] [-v VCF_FILE] [-p PEDIGREE] [-o OUTPUT_FILE]
                             [-w WINDOW_SIZE]

Analyse missing homozygote haplotypes to identify lethal recessives using phased population data  

optional arguments:  
  -h, --help            show this help message and exit  
  -v VCF_FILE, --vcf_file VCF_FILE  
                        phased VCF_file (compressed)  
  -p PEDIGREE, --pedigree PEDIGREE  
                        Pedigree (pig_id sire dam sex), space delimited  
  -o OUTPUT_FILE, --output_file OUTPUT_FILE  
                        Output file  
  -w WINDOW_SIZE, --window_size WINDOW_SIZE  
                        Window size to build haplotypes (bp)  

Example run:  ./pipeline_breeding_advise_EL.py -p VVVV_chr12.vcf.gz -p VVVV.pedigree -o VVVV_chr12.out -w 1000000

